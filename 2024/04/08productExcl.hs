{-
Given an array of integers, return a new array such that each element at index i
of the new array is the product of all the numbers in the original array except
the one at i.

For example, if our input was [1, 2, 3, 4, 5], the expected output would be
[120, 60, 40, 30, 24]. If our input was [3, 2, 1], the expected output would be
[2, 3, 6].

Follow-up: what if you can't use division?
-}

productExcl :: [Int] -> [Int]
productExcl lst = productExcl' [] lst where
  productExcl' _ []       = []
  productExcl' acc (x:xs) = [product $ acc ++ xs] ++ productExcl' (acc ++ [x]) xs


-- run productExcl with [1, 2, 3, 4, 5]
main :: IO ()
main = putStrLn . show $ productExcl [1..10]
