{-
Given the root to a binary tree, implement serialize(root), which serializes the
tree into a string, and deserialize(s), which deserializes the string back into
the tree.

For example, given the following Node class

```
class Node:
    def __init__(self, val, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right
```

The following test should pass:

```
node = Node('root', Node('left', Node('left.left')), Node('right'))
assert deserialize(serialize(node)).left.left.val == 'left.left'
```
-}

data Node = Node String (Maybe Node) (Maybe Node) deriving (Show, Read)

{- idk either
serialize :: Maybe Node -> String
serialize Just (Node v l r) = "[" ++ v ++ ":" ++ serialize l ++ ";" serialize r ++ "]"

deserialize :: [Char] -> Node
deserialize "" = Nothing
deserialize (x:xs)
-}
