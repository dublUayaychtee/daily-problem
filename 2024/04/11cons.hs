{-
cons(a, b) constructs a pair, and car(pair) and cdr(pair) returns
the first and last element of that pair.
For example, car(cons(3, 4)) returns 3, and cdr(cons(3, 4)) returns 4.

Given this implementation of cons:

def cons(a, b):
    def pair(f):
        return f(a, b)
    return pair

Implement car and cdr.
-}

cons :: t1 -> t2 -> (t1 -> t2 -> t3) -> t3
cons a b = (\f -> f a b)

car :: ((p1 -> p2 -> p1) -> t) -> t
car p = p (\a b -> a)
cdr :: ((p1 -> p2 -> p2) -> t) -> t
cdr p = p (\a b -> b)


-- run car and cdr
main :: IO ()
main = do
  putStrLn . show $ car (cons 3 4)
  putStrLn . show $ cdr (cons 3 4)
