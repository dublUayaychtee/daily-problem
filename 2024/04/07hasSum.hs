{-
Given a list of numbers and a number k, return whether any two numbers from the
list add up to k.

For example, given [10, 15, 3, 7] and k of 17, return true since 10 + 7 is 17.

Bonus: Can you do this in one pass?
-}

hasSum :: [Int] -> Int -> Bool
hasSum [] _ = False
hasSum (x:xs) k = or $ map (\a -> x + a == k) xs ++ [hasSum xs k]


-- run hasSum given [10, 15, 3, 7] and with k as 1 through 20
main :: IO ()
main = putStrLn . show $ map (\a -> hasSum [10,15,3,7] a) [1..20]
